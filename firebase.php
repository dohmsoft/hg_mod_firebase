<?php
/**
 * Créé par Dominique Nadeau.
 * (c) 2014-2018 dOhm Soft
 * Tous droits réservés.
 * Toute modification ou reproduction entraînera une poursuite judiciaire.
 * Date: 20-Feb-20
 * Heure: 20:34
 */

linker::addJS("https://www.gstatic.com/firebasejs/7.9.0/firebase-app.js");
class module_firebase extends base_module{

	function render(){
		?>
		<script>
		</script>
		<?php
	}
}